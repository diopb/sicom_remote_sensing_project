import numpy as np
import cv2



# Upsample methode
## Apply rescale on the multispectral image to have the shape of the pan image
def upsample(pan, hs):
    
    #upsample with bicubic
    new_sized = cv2.resize(hs, (pan.shape[1], pan.shape[0]), pan, interpolation=cv2.INTER_CUBIC)
    
    return new_sized


# Normalization
def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    assert x.shape==y.shape, "Images have not the same dimension"

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
        x_norm = y_std * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + y_mean
        return x_norm


# Several pansharpening method implementation
    
## Component Substitution

### Additive injection
def CS_add(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands
    w = 1/N                                         # Same value of the weights for each band
    g = 1                                           # No gain correction
    S = np.zeros(M.shape)
    I = np.zeros(P.shape)
    for i in range (N-1):
        I += M[:,:,i] * w 
    P = norm_std_img(P, I)                          # Normalisation of the Pan image to have the same range value of I
    for i in range (N):
        S[:,:,i] = M[:,:,i] + g*(P-I)
    return S

### Multiplicative injection
def CS_mult(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands
    w = 1/N                                         # Same value of the weights for each band
    S = np.zeros(M.shape)
    I = np.zeros(P.shape)
    for i in range (N):
        I += M[:,:,i] * w
    P = norm_std_img(P, I)                          # Normalisation of the Pan image to have the same range value of I
    for i in range (N):
        S[:,:,i] = np.multiply(M[:,:,i],np.divide(P,I))
    return S


## Low pass filtering

### Additive injection
def Low_pass_add_box(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands 
    g = 1
    hlp = np.ones((3,3),np.float32)/9
    S = np.zeros(M.shape)
    for i in range (N):
        S[:,:,i] = M[:,:,i] + g*(P-cv2.filter2D(P,-1,hlp))
    return S

def Low_pass_add_gauss(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands 
    g = 1
    hlp_size = (11,11)
    sigma = 1
    S = np.zeros(M.shape)
    for i in range (N):
        S[:,:,i] = M[:,:,i] + g*(P-cv2.GaussianBlur(P,hlp_size, sigma))
    return S

def Low_pass_add_Laplacian(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands 
    g = 1
    S = np.zeros(M.shape)
    for i in range (N):
        S[:,:,i] = M[:,:,i] + g*(P-cv2.Laplacian(P, -1))
    S = (S-np.min(S))/(np.max(S)-np.min(S))
    return S

### Multiplicative injection
def Low_pass_mult_box(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands 
    g = 1
    hlp = np.ones((3,3),np.float32)/9
    S = np.zeros(M.shape)
    for i in range (N):
        S[:,:,i] =np.multiply(M[:,:,i],np.divide(P,cv2.filter2D(P,-1,hlp)))
    return S

def Low_pass_mult_gauss(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands
    hlp_size = (11,11)
    sigma = 1
    S = np.zeros(M.shape)
    for i in range (N):
        S[:,:,i] = np.multiply(M[:,:,i],np.divide(P,cv2.GaussianBlur(P,hlp_size, sigma)))
    return S

def Low_pass_mult_Laplacian(P,M) :
    N = M.shape[-1]                                 # Number of spectral bands
    g = 1
    S = np.zeros(M.shape)
    for i in range (N):
        S[:,:,i] = np.multiply(M[:,:,i],np.divide(P,cv2.Laplacian(P, -1)))
    S = (S-np.min(S))/(np.max(S)-np.min(S))
    return S